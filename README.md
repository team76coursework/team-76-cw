# Football World Cup 2018
# Team Coursework Group 76

# Group Members
	- Anjumol Kochukarottu Varkey
	- Bala Pranay Reddy Yeruva
	- Gopi Krishna Guduru
	- Jijo Winny
	- Rennie Anurag Jannu

# Purpose
   This Repositery provides contents and details of data analysis of football players in the year 2018 while comparing with the data of the age and goals per minute.

# Data Set URL
   https://www.kaggle.com/sawya34/football-world-cup-2018-dataset#Players_Score.csv
